# The Nodes

![Godot Engine Badge](https://img.shields.io/badge/Godot%20Engine-478CBF?logo=godotengine&logoColor=fff&style=flat)
[![Revolt Badge](https://img.shields.io/badge/Revolt_Chat-ff4655?logo=revolt.chat&logoColor=fff&style=flat)](https://rvlt.gg/s2QvY9cJ)
[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

The Nodes is life simulation game. 

Take control of a world full of virtual people. 

Make decisions that affect their lives. 

Take care to ensure their success and survival. 

Create a beautiful home for your custom made nodes. 

Build lifelong relationships between nodes. 

Start and manage a family and descendants. 

Watch over your projected life.